<?php

namespace App\helpers;

use Symfony\Component\HttpKernel\KernelInterface;

class DataService
{

  public function __construct(private KernelInterface $kernel)
  {
    
  }
  public function getPrompt()
  {
    $json_data = file_get_contents($this->kernel->getProjectDir().'/public/data/prompt.json');
    return json_decode($json_data);
  }
}
