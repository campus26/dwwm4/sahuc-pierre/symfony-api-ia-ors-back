<?php

namespace App\helpers;

use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\JsonResponse;

class OpenRouteService
{

    public static function getCoordinates($ville)
    {
        $apiKey = '5b3ce3597851110001cf6248c512d188232a4078802a90c872371225';

        $client = new Client();
        $response = $client->request('GET', 'https://api.openrouteservice.org/geocode/search?api_key='.$apiKey.'&text='.$ville);
        $coords = json_decode($response->getBody()->getContents());
        return $coords->features[0]->geometry->coordinates;
    }

    public static function calculRoute($ville_depart, $ville_arrivee, $transport_mode, $language)
    {

        $requestdata = [
            "coordinates" => [[$ville_depart[1], $ville_depart[0]], [$ville_arrivee[1], $ville_arrivee[0]]], 
            "language" => $language,
            "radiuses" =>-1
            
        ];

        // var_dump($ville_depart[0]);
        // var_dump(json_encode($requestdata));


        // $routeQuerypost = 'https://api.openrouteservice.org/v2/directions/'.$transport_mode.'/json';
        $routeQuerypost = 'https://api.openrouteservice.org/v2/directions/'.$transport_mode;

        $options = [
            'headers' => [
              "Content-Type" => "application/json",
              "Authorization" => "5b3ce3597851110001cf6248c512d188232a4078802a90c872371225"
            ],
            'body' => json_encode($requestdata),  
            // 'body' => '{"coordinates":[[8.681495,49.41461],[8.686507,49.41943],[8.687872,49.420318]],"language":"fr-fr","radiuses":-1}',
        ];
        

        $client = new \GuzzleHttp\Client();

        $response = $client->post($routeQuerypost, $options);

        $responseData = $response->getBody()->getContents();

        $responseData = json_decode($responseData);

        return $responseData;
    }

}