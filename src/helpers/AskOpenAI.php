<?php

namespace App\helpers;

use GuzzleHttp\Client;

use OpenAI;

class AskOpenAI
{
    public static function client($requete)
    {
        $apiKeyOpenAI = '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';

        $client = OpenAI::client($apiKeyOpenAI);
        $result = $client->chat()->create([
            'model' => 'gpt-3.5-turbo',
            'messages' => [
                $requete
            ],
        ]);
       
       return json_decode($result->choices[0]->message->content);
    }
}