<?php

namespace App\helpers;

use GuzzleHttp\Client;

use OpenAI;

class AskMistral
{

    public static function client($requete, $kernel)
    {
        $apiKey = 'ss8ywEaPiLICdPWq7fuqvbPnYyc2tAaZ';
        $apiUrl = 'https://api.mistral.ai/v1/chat/completions';
        $cacertPath = $kernel->getProjectDir().'/config/cacert.pem';
        
        $data = [
            'model' => 'mistral-large-latest',
            // 'model' => 'ada',

            'messages' => [
                [
                    'role' => 'user',
                    'content' => $requete
                ],
            ],
            "temperature"=> 0.7,
            "top_p" => 1,
            "max_tokens" => 512,
            "stream" => false,
            "safe_prompt" => false,
            "random_seed"=> 1337
        ];

        $options = [
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . $apiKey,
            ],
            'json' => $data,
            'verify' => $cacertPath,
        ];

        $client = new \GuzzleHttp\Client();

        return $client->post($apiUrl, $options);
    }

}