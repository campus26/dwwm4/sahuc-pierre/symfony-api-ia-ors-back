<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\HttpFoundation\Request;

use App\helpers\DataService;
use App\helpers\OpenRouteService;
use App\helpers\AskOpenAI;
use App\helpers\AskMistral;

class ApiController extends AbstractController
{
    private $ville_depart = 'lyon';
    private $ville_arrivee = 'nice';
    private $nb_hebergement = 3;
    private $type_hebergement = 'chambres d hôtes';
    private $budget_max_hebergement = 100;
    private $nb_activites = 3;
    private $type_activites = 'sportive';
    private $ville_intermediaires = [""];
    private $restauration = false;
    private $structureJSON = 
        '{ 
            "ville_depart": {"nom": "nom", "lat": "lat", "lon": "lon"}, 
            "ville_arrivee": {"nom": "nom", "lat": "lat", "lon": "lon"}, 
            "etapes": {"etape1": {"ville_etape": "ville_etape", "lat": "lat", "lon": "lon", "hebergement": [{"nom": "nom", "prix": "prix", "adresse": "adresse"}, ...], "activites": [{"nom": "nom", "description": "description"}, ...] }, "etape2": ... }
        }';
    private $structureJSONResto = 
    '{ 
        "ville_depart": {"nom": "nom", "lat": "lat", "lon": "lon"}, 
        "ville_arrivee": {"nom": "nom", "lat": "lat", "lon": "lon"}, 
        "etapes": {"etape1": {"ville_etape": "ville_etape", "lat": "lat", "lon": "lon", "hebergement": [{"nom": "nom", "prix": "prix", "adresse": "adresse"}, ...], "activites": [{"nom": "nom", "description": "description"}, ...], "restaurant": [{"nom": "nom", "tarif": "tarif"}, ...] }, "etape2": ... }
    }';

    #[Route('/api-mistral', name: 'app_api_mistral')]
    public function index(KernelInterface $kernel): JsonResponse
    {

        $requete = 'Conçoit un voyage de '.$this->ville_depart.' à '.$this->ville_arrivee.'. Le voyage durera '.$this->nb_etapes.' jours. Propose une ville étape par jour avec ses coordonnées géographiques. 
            Pour chaque étape propose '. $this->nb_hebergement. ' hébergements de type '.$this->type_hebergement. ' dont le budget maximum sera inférieur ou égal à '.$this->budget_max_hebergement. ' euros. 
            Propose aussi '. $this->nb_activites. ' activités de type '.$this->type_activites;//. '. Structure la réponse en json de la façon suivante :'.$this->structureRetourJSON;


        // $requete_itineraire = 'Conçoit un voyage de '.$this->ville_depart.' à '.$this->ville_arrivee.'. Le voyage durera '.$this->nb_etapes.' jours. Propose une ville étape par jour avec ses coordonnées géographiques. Structure la réponse en json de la façon suivante :'.$this->structureRetourJSONParcours;
      
        $requete_programme_etape = 'A lyon propose '. $this->nb_hebergement. ' hébergements de type '.$this->type_hebergement. ' dont le budget maximum sera inférieur ou égal à '.$this->budget_max_hebergement. ' euros. 
        Propose aussi '. $this->nb_activites. ' activités de type '.$this->type_activites.'Strcuture la réponse en json de la méthode suivante : { "ville_etape": "ville_etape", "lat": "lat", "lon": "lon", "hebergement": [{"nom": "nom", "prix": "prix", "Adress": "adresse"}, ...], "activites": [{"nom": "nom", "description": "description"}, ...] }';

        $response = AskMistral::client($requete_programme_etape, $kernel);

        $responseData = $response->getBody()->getContents();

        $responseData = json_decode($responseData);

        dd($responseData->choices[0]->message->content);

        return $this->json($responseData->choices[0]->message->content);
    }


    #[Route('/api-openai', name: 'app_openai')]
    public function api(DataService $data, Request $request): JsonResponse
    {

        $donnees = json_decode($request->getContent(), true);

        $this->ville_depart = $donnees['depart'];
        $this->ville_arrivee = $donnees['arrivee'];
        $this->nb_hebergement = $donnees['nb_hebergement'];
        $this->type_hebergement = $donnees['type_hebergement'];
        $this->budget_max_hebergement = $donnees['budget_max_hebergement'];
        $this->nb_activites = $donnees['nb_activites'];
        $this->type_activites = $donnees['type_activites'];
        $this->ville_intermediaires = $donnees['villes_intermediaires'];  
        $this->restauration = $donnees['restauration'];

        if ($this->ville_intermediaires) {
            if ($this->restauration) {
                $requete = [
                    'role' => 'user',
                    'content' => 'Conçoit un voyage de '.$this->ville_depart.' à '.$this->ville_arrivee.'. Le voyage devra faire étape dans les villes suivantes : '.$this->ville_intermediaires. '. Tu donneras leurs coordonnées géographiques. La dernière étape se situera entre la dernière des villes intermédiaires et la ville d arrivée. Pour chaque étape propose '. $this->nb_hebergement. ' hébergements de type '.$this->type_hebergement.'Leur budget maximum sera inférieur ou égal à '.$this->budget_max_hebergement. ' euros. Pour chaque étape, propose aussi '. $this->nb_activites. ' activités de type '.$this->type_activites.' ainsi que la liste des trois meilleurs restaurants de la ville avec leur nom et leur tarif moyen. Structure la réponse en json de la façon suivante :'.$this->structureJSONResto.' Tu feras attention à strictement respecter la casse des différents variables'
                ];
            } else {
                $requete = [
                    'role' => 'user',
                    'content' => 'Conçoit un voyage de '.$this->ville_depart.' à '.$this->ville_arrivee.'. Le voyage devra faire étape dans les villes suivantes : '.$this->ville_intermediaires. '. Tu donneras leurs coordonnées géographiques. La dernière étape se situera entre la dernière des villes intermédiaires et la ville d arrivée. Pour chaque étape propose '. $this->nb_hebergement. ' hébergements de type '.$this->type_hebergement.'Leur budget maximum sera inférieur ou égal à '.$this->budget_max_hebergement. ' euros. Pour chaque étape, propose aussi '. $this->nb_activites. ' activités de type '.$this->type_activites.'. Structure la réponse en json de la façon suivante :'.$this->structureJSON.' Tu feras attention à strictement respecter la casse des différents variables'
                ];
            }
        } else {
            if ($this->restauration) {
                $requete = [
                    'role' => 'user',
                    'content' => 'Conçoit un voyage de '.$this->ville_depart.' à '.$this->ville_arrivee.'. Dans la ville d arrivée, tu proposeras '. $this->nb_hebergement. ' hébergements de type '.$this->type_hebergement.'Leur budget maximum sera inférieur ou égal à '.$this->budget_max_hebergement. ' euros. Propose aussi '. $this->nb_activites. ' activités de type '.$this->type_activites.' ainsi que la liste des trois meilleurs restaurants de la ville avec leur nom et leur tarif moyen. Structure la réponse en json de la façon suivante :'.$this->structureJSONResto.' Tu feras attention à strictement respecter la casse des différents variables'
                ];
            } else {
                $requete = [
                    'role' => 'user',
                    'content' => 'Conçoit un voyage de '.$this->ville_depart.' à '.$this->ville_arrivee.'. Dans la ville d arrivée, tu proposeras '. $this->nb_hebergement. ' hébergements de type '.$this->type_hebergement.'Leur budget maximum sera inférieur ou égal à '.$this->budget_max_hebergement. ' euros. Propose aussi '. $this->nb_activites. ' activités de type '.$this->type_activites.'. Structure la réponse en json de la façon suivante :'.$this->structureJSON.' Tu feras attention à strictement respecter la casse des différents variables'
                ];
            }
        }

        // Appel réel à l'api OpenAI
        if ($requete) {
            $jsonResult = AskOpenAI::client($requete);
        } else {
            $jsonResult = "Veuillez donner des informations valides";
        }

        // Fake Data pour test
        // $jsonResult = $data->getPrompt();

        if (isset($jsonResult->ville_depart->lat)) 
            {
                $coordsPrev = [$jsonResult->ville_depart->lat, $jsonResult->ville_depart->lon];
                $etapes = $jsonResult->etapes;
                foreach ($etapes as $jour) {
                    $coords = [$jour->lat, $jour->lon];
                    $track = OpenRouteService::calculRoute($coordsPrev, $coords, 'driving-car', 'fr-fr');
                    $jour->polyline = $track;
                    $coordsPrev = $coords;
                }
            } else {
                $responseData = new \stdClass();
                $responseData->error = "Veuillez donner des informations valides";
            }

        $responseData = $jsonResult;

        if ($requete) $responseData->requete = $requete;

        return new JsonResponse($responseData);
    
    }

  
}