## README Travel IA Back

Pour récupérer le projet en local, faire :
```
git@gitlab.com:campus26/dwwm4/sahuc-pierre/symfony-api/symfony-api-back.git
```

Ensuite on récupère toutes les dépendances et bundle associés avec l'instruction :
```
composer require
```

Le lancement du projet en local se fait avec l'instruction :
```
symfony serve
```

Les requêtes HTTP à l'API sont filtrés par le bundle Nelmio. C'est la propriété allow_origin dans le fichier nelmio_cors.yaml qui détermine les domaines autorisés à passer des requêtes. La valeur de cette propriété étant attribuée via le fichier .env, il faudra se rendre dans ce fichier pour adapter la valeur de allow_origin en fonction de différent contexte de front. Dans notre contexte local, pour gérer différentes configuration de port, on renseigne :
```
CORS_ALLOW_ORIGIN_LOCAL='^https?:\/\/(localhost|127\.0\.0\.1)(:[0-9]+)?$'
```

